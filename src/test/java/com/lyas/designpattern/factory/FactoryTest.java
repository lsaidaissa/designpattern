package com.lyas.designpattern.factory;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.lyas.designpattern.decorator.Circle;
import com.lyas.designpattern.decorator.Rectangle;
import com.lyas.designpattern.factory.Factory;

public class FactoryTest {

	@Test
	public void getFactory() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
		assertTrue(Factory.getShapeFactory("Rectangle") instanceof Rectangle);
	}

	@Test
	public void getFactory2() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
		assertTrue(Factory.getShapeFactory("Circle") instanceof Circle);
	}

	@Test(expected = ClassNotFoundException.class)
	public void getFactory3() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
		assertTrue(Factory.getShapeFactory("aaaa") instanceof Circle);
	}
}
