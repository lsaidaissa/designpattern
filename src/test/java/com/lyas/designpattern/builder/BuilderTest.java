package com.lyas.designpattern.builder;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.lyas.designpattern.builder.Coca;
import com.lyas.designpattern.builder.Fruit;
import com.lyas.designpattern.builder.Mayonnaise;
import com.lyas.designpattern.builder.Meal;
import com.lyas.designpattern.builder.MealBuilder;
import com.lyas.designpattern.builder.Patisserie;
import com.lyas.designpattern.builder.Pepsi;

public class BuilderTest {
	@Test
	public void builderWithProducts() {
		Meal meal = new MealBuilder().add(new Coca("Coca zero", 2, 0.33)).add(new Fruit("Fekkous", 1))
				.add(new Patisserie("tarte", 1.5)).add(new Mayonnaise("algerienne", 0.5)).build();
		meal.getName();
		assertEquals(meal.getPrice(), 5.0);
	}

	@Test
	public void builderWithBoisson() {
		Meal meal = new MealBuilder().add(new Coca("Coca light", 2.5, 0.5)).add(new Pepsi("Pepsi sucre", 1, 0.33))
				.build();
		meal.getName();
		assertEquals(meal.getPrice(), 3.5);
	}

	@Test
	public void builderWithZeroProducts() {
		Meal meal = new MealBuilder().build();
		meal.getName();
		assertEquals(meal.getPrice(), 0.0);
	}
}
