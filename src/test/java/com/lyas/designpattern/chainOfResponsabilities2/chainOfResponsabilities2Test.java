package com.lyas.designpattern.chainOfResponsabilities2;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.lyas.designpattern.chainOfResponsabilities2.AbstractPurchase;
import com.lyas.designpattern.chainOfResponsabilities2.FactoryPurchase;
import com.lyas.designpattern.chainOfResponsabilities2.PurchaseRequest;

public class chainOfResponsabilities2Test {
	@Test
	public void factoryPurchase() {
		AbstractPurchase loggerChain = FactoryPurchase.getFactory();

		loggerChain.processRequest(new PurchaseRequest(500, "petit achat"));

		loggerChain.processRequest(new PurchaseRequest(570, "moyen achat"));

		loggerChain.processRequest(new PurchaseRequest(700, "grand achat"));

		assertTrue(true);
	}
}
