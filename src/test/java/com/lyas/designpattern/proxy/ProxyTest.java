package com.lyas.designpattern.proxy;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.lyas.designpattern.decorator.COLOR;
import com.lyas.designpattern.decorator.Circle;
import com.lyas.designpattern.decorator.ColoredShape;
import com.lyas.designpattern.decorator.Shape;
import com.lyas.designpattern.proxy.Proxy;

public class ProxyTest {
	Shape circ;
	ColoredShape coloredCircle;
	Proxy proxy;

	@Before
	public void instanciate() {
		circ = new Circle();
		coloredCircle = new ColoredShape(circ, COLOR.BLACK);
		proxy = new Proxy(coloredCircle);
	}

	@Test
	public void drawColoredCircle() {
		assertTrue("Hi, I'm a circle colored with : BLACK".equals(proxy.draw()));
	}

	@Test
	public void descriptionColoredCircle() {
		assertTrue("My Description is a Circle colored with : BLACK".equals(proxy.description()));
	}

}
