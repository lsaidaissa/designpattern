package com.lyas.designpattern.chainOfResponsabilities;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.lyas.designpattern.chainOfResponsabilities.AbstractLogger;
import com.lyas.designpattern.chainOfResponsabilities.FactoryLogger;

public class chainOfResponsabilitiesTest {
	@Test
	public void factoryLogger() {
		AbstractLogger loggerChain = FactoryLogger.getLogger();

		loggerChain.processRequest(AbstractLogger.INFO, "This is an information.");

		loggerChain.processRequest(AbstractLogger.DEBUG, "This is a debug level information.");

		loggerChain.processRequest(AbstractLogger.ERROR, "This is an error information.");
		assertTrue(true);
	}
}
