package com.lyas.designpattern.singleton;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SingletonTest {

	@Test
	public void singletonTest() {
		assertEquals(Singleton.getInstance(), Singleton.getInstance());
	}
}
