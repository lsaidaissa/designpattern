package com.lyas.designpattern.strategy;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ContextStrategyTest {
	@Test
	public void operationAddition() {
		Operation a = new AdditionOperation();
		assertEquals(1.0, new ContextStrategy(a).traitement(1.0, 0.0));
	}

	@Test
	public void operationSoustraction() {
		Operation a = new SoustractionOperation();
		assertEquals(1.0, new ContextStrategy(a).traitement(2.0, 1.0));
	}
}
