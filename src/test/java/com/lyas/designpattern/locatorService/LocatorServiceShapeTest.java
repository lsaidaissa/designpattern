package com.lyas.designpattern.locatorService;

import static org.junit.Assert.assertTrue;

import java.util.Objects;

import org.junit.Test;

import com.lyas.designpattern.decorator.Shape;
import com.lyas.designpattern.locatorService.LocatorServiceShape;

public class LocatorServiceShapeTest {
	@Test
	public void getShape() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
		Shape rectangle = LocatorServiceShape.getShape("Rectangle");
		assertTrue("Hi, I'm a rectangle".equals(rectangle.draw()));
	}

	@Test
	public void getShape2() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
		Shape rectangle = LocatorServiceShape.getShape("Rectangle");
		Shape rectangle1 = LocatorServiceShape.getShape("Rectangle");
		assertTrue(Objects.equals(rectangle, rectangle1));
	}
}
