package com.lyas.designpattern.decorator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.lyas.designpattern.decorator.Circle;
import com.lyas.designpattern.decorator.Rectangle;
import com.lyas.designpattern.decorator.STYLE;
import com.lyas.designpattern.decorator.Shape;
import com.lyas.designpattern.decorator.StyledShape;

public class StyledShapeTest {

	Shape circ;
	Shape rect;
	StyledShape styledCircle;
	StyledShape styledRect;

	@Before
	public void instanciate() {
		circ = new Circle();
		rect = new Rectangle();
		styledCircle = new StyledShape(circ, STYLE.SOLID);
		styledRect = new StyledShape(rect, STYLE.DASH);
	}

	@Test
	public void constructorStyledCircle() {
		assertNotNull(styledCircle.getShape());
		assertEquals(STYLE.SOLID, styledCircle.getStyle());
	}

	@Test
	public void constructorStyledRect() {
		assertNotNull(styledRect.getShape());
		assertEquals(STYLE.DASH, styledRect.getStyle());
	}

	@Test
	public void drawStyledCircle() {
		assertTrue("Hi, I'm a circle styled with : SOLID".equals(styledCircle.draw()));
	}

	@Test
	public void drawStyledRect() {
		assertTrue("Hi, I'm a rectangle styled with : DASH".equals(styledRect.draw()));
	}

	@Test
	public void descriptionStyledCircle() {
		assertTrue("My Description is a Circle styled with : SOLID".equals(styledCircle.description()));
	}

	@Test
	public void descriptionStyledRect() {
		assertTrue("My Description is a Rectangle styled with : DASH".equals(styledRect.description()));
	}
}
