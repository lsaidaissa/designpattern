package com.lyas.designpattern.decorator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.lyas.designpattern.decorator.COLOR;
import com.lyas.designpattern.decorator.Circle;
import com.lyas.designpattern.decorator.ColoredShape;
import com.lyas.designpattern.decorator.Rectangle;
import com.lyas.designpattern.decorator.Shape;

public class ColoredShapeTest {
	Shape circ;
	Shape rect;
	ColoredShape coloredCircle;
	ColoredShape coloredRect;

	@Before
	public void instanciate() {
		circ = new Circle();
		rect = new Rectangle();
		coloredCircle = new ColoredShape(circ, COLOR.BLACK);
		coloredRect = new ColoredShape(rect, COLOR.BLUE);
	}

	@Test
	public void constructorStyledCircle() {
		assertNotNull(coloredCircle.getShape());
		assertEquals(COLOR.BLACK, coloredCircle.getColor());
	}

	@Test
	public void constructorStyledRect() {
		assertNotNull(coloredRect.getShape());
		assertEquals(COLOR.BLUE, coloredRect.getColor());
	}

	@Test
	public void drawStyledCircle() {
		assertTrue("Hi, I'm a circle colored with : BLACK".equals(coloredCircle.draw()));
	}

	@Test
	public void drawStyledRect() {
		assertTrue("Hi, I'm a rectangle colored with : BLUE".equals(coloredRect.draw()));
	}

	@Test
	public void descriptionStyledCircle() {
		assertTrue("My Description is a Circle colored with : BLACK".equals(coloredCircle.description()));
	}

	@Test
	public void descriptionStyledRect() {
		assertTrue("My Description is a Rectangle colored with : BLUE".equals(coloredRect.description()));
	}
}
