package com.lyas.designpattern.strategy;

public interface Operation {
	double traitement(double a, double b);
}
