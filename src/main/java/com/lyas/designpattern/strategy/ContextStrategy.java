package com.lyas.designpattern.strategy;

public class ContextStrategy {
	private Operation operation;

	public ContextStrategy(Operation operation) {
		this.operation = operation;
	}

	public double traitement(double a, double b) {
		return operation.traitement(a, b);
	}
}
