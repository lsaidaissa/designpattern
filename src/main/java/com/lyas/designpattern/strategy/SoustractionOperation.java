package com.lyas.designpattern.strategy;

public class SoustractionOperation implements Operation {

	public double traitement(double a, double b) {
		return a - b;
	}

}
