package com.lyas.designpattern.chainOfResponsabilities;

public class ConsoleLogger extends AbstractLogger {

	public ConsoleLogger(int level) {
		super(level);
	}

	@Override
	public void write(String message) {
		System.out.println("Console Logger : " + message);
	}

}
