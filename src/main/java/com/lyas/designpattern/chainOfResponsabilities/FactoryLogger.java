package com.lyas.designpattern.chainOfResponsabilities;

public class FactoryLogger {
	public static AbstractLogger getLogger() {
		AbstractLogger consoleLogger = new ConsoleLogger(AbstractLogger.INFO);
		AbstractLogger fileLogger = new FileLogger(AbstractLogger.DEBUG);
		AbstractLogger errorLogger = new ErrorLogger(AbstractLogger.ERROR);
		errorLogger.setSuccessor(fileLogger);
		fileLogger.setSuccessor(consoleLogger);
		return errorLogger;
	}
}
