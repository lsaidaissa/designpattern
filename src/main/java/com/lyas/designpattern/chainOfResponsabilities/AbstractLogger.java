package com.lyas.designpattern.chainOfResponsabilities;

public abstract class AbstractLogger {
	public final static int ERROR = 3;
	public final static int DEBUG = 2;
	public final static int INFO = 1;
	protected int level;

	protected AbstractLogger nextLogger;

	public AbstractLogger(int level) {
		this.level = level;
	}

	public void setSuccessor(AbstractLogger nextLogger) {
		this.nextLogger = nextLogger;
	}

	public void processRequest(int level, String message) {
		if (this.level <= level)
			write(message);
		if (nextLogger != null)
			nextLogger.processRequest(level, message);
	}

	public abstract void write(String message);

}
