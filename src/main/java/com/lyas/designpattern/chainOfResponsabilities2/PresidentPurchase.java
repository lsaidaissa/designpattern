package com.lyas.designpattern.chainOfResponsabilities2;

public class PresidentPurchase extends AbstractPurchase {

	@Override
	public double getAllowed() {
		return super.BASE + (super.BASE * 0.5);
	}

	@Override
	public String getRole() {
		return "President";
	}

}
