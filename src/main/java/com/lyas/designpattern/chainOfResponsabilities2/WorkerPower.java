package com.lyas.designpattern.chainOfResponsabilities2;

public class WorkerPower extends AbstractPurchase {

	@Override
	public double getAllowed() {
		return super.BASE + (super.BASE * 0.1);
	}

	@Override
	public String getRole() {
		return "Worker";
	}

}
