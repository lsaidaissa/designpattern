package com.lyas.designpattern.chainOfResponsabilities2;

public class ManagerPurchase extends AbstractPurchase {

	@Override
	public double getAllowed() {
		return super.BASE + (super.BASE * 0.2);
	}

	@Override
	public String getRole() {
		return "Manager";
	}

}
