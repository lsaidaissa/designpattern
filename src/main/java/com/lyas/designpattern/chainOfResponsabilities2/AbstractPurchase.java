package com.lyas.designpattern.chainOfResponsabilities2;

public abstract class AbstractPurchase {

	protected final double BASE = 500;

	private AbstractPurchase successor;

	public void setSuccessor(AbstractPurchase successor) {
		this.successor = successor;
	}

	public void processRequest(PurchaseRequest request) {
		if (this.getAllowed() >= request.getAmount()) {
			System.out.println("The request : " + request.getPurpose() + ", with the mount : " + request.getAmount()
					+ "" + ", is hold by " + this.getRole() + ", because his max is : " + this.getAllowed());
		} else {
			if (successor != null) {
				successor.processRequest(request);
			}
		}
	}

	public abstract double getAllowed();

	public abstract String getRole();
}
