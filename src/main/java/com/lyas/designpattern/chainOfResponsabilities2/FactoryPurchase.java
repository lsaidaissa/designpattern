package com.lyas.designpattern.chainOfResponsabilities2;

public class FactoryPurchase {
	public static AbstractPurchase getFactory() {
		AbstractPurchase worker = new WorkerPower();
		AbstractPurchase manager = new ManagerPurchase();
		AbstractPurchase president = new PresidentPurchase();
		worker.setSuccessor(manager);
		manager.setSuccessor(president);
		return worker;
	}
}
