package com.lyas.designpattern.decorator;

public class StyledShape extends ShapeDecorator {

	private STYLE style;

	public StyledShape(Shape shape, STYLE style) {
		super(shape);
		this.style = style;
	}

	public String draw() {
		return super.shape.draw() + " styled with : " + style;
	}

	public String description() {
		return super.shape.description() + " styled with : " + style;
	}

	public STYLE getStyle() {
		return this.style;
	}

}
