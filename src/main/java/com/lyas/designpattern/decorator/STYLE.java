package com.lyas.designpattern.decorator;

public enum STYLE {
	SOLID, DASH, DOT, DOUBLE_DASH, DASH_SPACE
}
