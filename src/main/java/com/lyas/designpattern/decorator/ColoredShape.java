package com.lyas.designpattern.decorator;

public class ColoredShape extends ShapeDecorator {

	private COLOR color;

	public ColoredShape(Shape shape, COLOR color) {
		super(shape);
		this.color = color;
	}

	public String draw() {
		return super.shape.draw() + " colored with : " + this.color;
	}

	public String description() {
		return super.shape.description() + " colored with : " + this.color;
	}

	public COLOR getColor() {
		return this.color;
	}

}
