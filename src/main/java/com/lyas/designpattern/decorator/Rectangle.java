package com.lyas.designpattern.decorator;

public class Rectangle implements Shape {

	public String draw() {
		return "Hi, I'm a rectangle";
	}

	public String description() {
		return "My Description is a Rectangle";
	}

}
