package com.lyas.designpattern.decorator;

public interface Shape {
	String draw();

	String description();
}
