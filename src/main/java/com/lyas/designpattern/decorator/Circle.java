package com.lyas.designpattern.decorator;

public class Circle implements Shape {

	public String draw() {
		return "Hi, I'm a circle";
	}

	public String description() {
		return "My Description is a Circle";
	}

}
