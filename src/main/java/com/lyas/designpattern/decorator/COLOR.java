package com.lyas.designpattern.decorator;

public enum COLOR {
	RED, GREEN, BLUE, YELLOW, WHITE, BLACK, ORANGE, MAROON
}
