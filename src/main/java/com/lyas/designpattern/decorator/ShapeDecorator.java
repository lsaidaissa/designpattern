package com.lyas.designpattern.decorator;

public abstract class ShapeDecorator implements Shape {
	protected Shape shape;

	public ShapeDecorator(Shape shape) {
		this.shape = shape;
	}

	public Shape getShape() {
		return this.shape;
	}
}
