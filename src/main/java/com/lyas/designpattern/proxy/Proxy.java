package com.lyas.designpattern.proxy;

import com.lyas.designpattern.decorator.Shape;
import com.lyas.designpattern.decorator.ShapeDecorator;

public class Proxy implements Shape {

	private ShapeDecorator shapeDecorator;

	public Proxy(ShapeDecorator shapeDecorator) {
		this.shapeDecorator = shapeDecorator;
	}

	public String draw() {
		return this.shapeDecorator.draw();
	}

	public String description() {
		return this.shapeDecorator.description();
	}

	public void setShapeDecorator(ShapeDecorator shapeDecorator) {
		this.shapeDecorator = shapeDecorator;
	}

	public ShapeDecorator getShapeDecorator() {
		return shapeDecorator;
	}

}
