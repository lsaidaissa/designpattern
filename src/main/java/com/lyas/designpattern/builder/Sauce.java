package com.lyas.designpattern.builder;

public abstract class Sauce extends Product {

	protected Sauce(String name, double price) {
		super(name, price);
	}

	@Override
	public String toString() {
		return "Sauce [name=" + getName() + ", price=" + getPrice() + "]";
	}

}
