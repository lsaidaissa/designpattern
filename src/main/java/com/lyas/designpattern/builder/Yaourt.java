package com.lyas.designpattern.builder;

public class Yaourt extends Dessert {

	public Yaourt(String name, double price) {
		super(name, price);
	}

	public String toString() {
		return "Yaourt : " + super.toString();
	}

}
