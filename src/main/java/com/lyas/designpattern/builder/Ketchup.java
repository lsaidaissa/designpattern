package com.lyas.designpattern.builder;

public class Ketchup extends Sauce {

	public Ketchup(String name, double price) {
		super(name, price);
	}

	public String toString() {
		return "Ketchup : " + super.toString();
	}

}
