package com.lyas.designpattern.builder;

public class Mayonnaise extends Sauce {

	public Mayonnaise(String name, double price) {
		super(name, price);
	}

	public String toString() {
		return "Mayonnaise : " + super.toString();
	}

}
