package com.lyas.designpattern.builder;

public class Hrissa extends Sauce {

	public Hrissa(String name, double price) {
		super(name, price);
	}

	public String toString() {
		return "Hrissa : " + super.toString();
	}

}
