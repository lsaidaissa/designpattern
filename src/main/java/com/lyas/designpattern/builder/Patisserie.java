package com.lyas.designpattern.builder;

public class Patisserie extends Dessert {

	public Patisserie(String name, double price) {
		super(name, price);
	}

	public String toString() {
		return "Patisserie : " + super.toString();
	}

}
