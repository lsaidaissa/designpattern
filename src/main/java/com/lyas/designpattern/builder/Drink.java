package com.lyas.designpattern.builder;

public abstract class Drink extends Product {

	protected double quantite;

	protected Drink(String name, double price) {
		super(name, price);
	}

	protected Drink(String name, double price, double quantite) {
		super(name, price);
		this.quantite = quantite;
	}

	public double getQuantite() {
		return quantite;
	}

	public void setQuantite(double quantite) {
		this.quantite = quantite;
	}

	@Override
	public String toString() {
		return "Boisson [name=" + super.getName() + ", price =" + super.getPrice() + ", quantite=" + quantite + "]";
	}

}
