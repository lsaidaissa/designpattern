package com.lyas.designpattern.builder;

public class Hamoud extends Drink {

	public Hamoud(String name, double price) {
		super(name, price);
	}

	public Hamoud(String name, double price, double quantite) {
		super(name, price, quantite);
	}

	public String toString() {
		return "Hamoud : " + super.toString();
	}
}
