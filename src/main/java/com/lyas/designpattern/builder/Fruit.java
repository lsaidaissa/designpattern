package com.lyas.designpattern.builder;

public class Fruit extends Dessert {

	public Fruit(String name, double price) {
		super(name, price);
	}

	public String toString() {
		return "Fruit : " + super.toString();
	}

}
