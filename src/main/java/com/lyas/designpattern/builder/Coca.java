package com.lyas.designpattern.builder;

public class Coca extends Drink {

	public Coca(String name, double price) {
		super(name, price);
	}

	public Coca(String name, double price, double quantite) {
		super(name, price, quantite);
	}

	public String toString() {
		return "Coca : " + super.toString();
	}

}
