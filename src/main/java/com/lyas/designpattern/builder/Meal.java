package com.lyas.designpattern.builder;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Meal {
	private Set<Product> products = new HashSet<Product>();

	public Set<Product> getProducts() {
		return products;
	}

	public void setProduits(Set<Product> produits) {
		this.products = produits;
	}

	public double getPrice() {
		double price = 0;
		Iterator<Product> i = products.iterator();
		while (i.hasNext()) {
			price += i.next().getPrice();
		}
		return price;
	}

	public void getName() {
		Iterator<Product> i = products.iterator();
		System.out.println("******************** Items list *************************");
		while (i.hasNext()) {
			System.out.println(i.next().toString());
		}
		System.out.println("**** Thank you for your visite, see you soon ************");
	}
}
