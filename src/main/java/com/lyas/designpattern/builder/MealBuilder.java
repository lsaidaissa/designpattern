package com.lyas.designpattern.builder;

public class MealBuilder {
	private Meal meal = new Meal();

	public MealBuilder add(Product produit) {
		this.meal.getProducts().add(produit);
		return this;
	}

	public Meal build() {
		return this.meal;
	}

}
