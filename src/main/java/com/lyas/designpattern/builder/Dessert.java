package com.lyas.designpattern.builder;

public abstract class Dessert extends Product {

	protected Dessert(String name, double price) {
		super(name, price);
	}

	@Override
	public String toString() {
		return "Dessert [name=" + getName() + ", price=" + getPrice() + "]";
	}

}
