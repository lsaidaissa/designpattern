package com.lyas.designpattern.builder;

public class Pepsi extends Drink {

	public Pepsi(String name, double price) {
		super(name, price);
	}

	public Pepsi(String name, double price, double quantite) {
		super(name, price, quantite);
	}

	public String toString() {
		return "Pepsi : " + super.toString();
	}
}
