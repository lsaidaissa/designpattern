package com.lyas.designpattern.factory;

import com.lyas.designpattern.decorator.Shape;

public class Factory {
	public static Shape getShapeFactory(String str)
			throws IllegalAccessException, ClassNotFoundException, InstantiationException {
		String classPath = new StringBuilder("com.lyas.designpattern.decorator.").append(str).toString();
		return (Shape) Class.forName(classPath).newInstance();
	}
}
