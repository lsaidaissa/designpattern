package com.lyas.designpattern.locatorService;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import com.lyas.designpattern.decorator.Shape;

public class CacheShape {
	private List<Shape> shapes = new ArrayList<Shape>();

	public void addShape(Shape shape) {
		this.shapes.add(shape);
	}

	public Object getShape(String name) {
		ListIterator<Shape> i = shapes.listIterator();
		while (i.hasNext()) {
			Shape s = i.next();
			if (name.equals(s.getClass().getSimpleName())) {
				return s;
			}
		}
		return null;
	}
}
