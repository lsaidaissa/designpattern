package com.lyas.designpattern.locatorService;

import com.lyas.designpattern.decorator.Shape;
import com.lyas.designpattern.factory.Factory;

public class LocatorServiceShape {
	private static CacheShape cache = new CacheShape();

	public static Shape getShape(String name)
			throws IllegalAccessException, ClassNotFoundException, InstantiationException {
		Shape local = (Shape) cache.getShape(name);
		if (local != null) {
			return local;
		}
		local = Factory.getShapeFactory(name);
		cache.addShape(local);
		return local;
	}
}
